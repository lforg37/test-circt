#include <array>
#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <system_error>
#include <tuple>

#include "circt/Dialect/FIRRTL/FIRRTLAttributes.h"
#include "circt/Dialect/FIRRTL/FIRRTLDialect.h"
#include "circt/Dialect/FIRRTL/FIRRTLOpInterfaces.h"
#include "circt/Dialect/FIRRTL/FIRRTLOps.h"

#include "circt/Dialect/FIRRTL/FIRRTLTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinTypes.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/OwningOpRef.h"
#include "mlir/IR/Value.h"
#include "llvm/Support/raw_ostream.h"


using std::uint32_t;
using namespace circt;

int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " Input_width Shift_size" << std::endl;
        return EXIT_FAILURE;
    }
    uint32_t input_size = std::atoi(argv[1]);
    uint32_t shift_size = std::atoi(argv[2]);

    uint32_t output_size = input_size - 1 + (uint32_t{1} << shift_size);
    
    // MLIR Bureaucracy
    mlir::MLIRContext context;
    context.loadDialect<firrtl::FIRRTLDialect>();
    mlir::OpBuilder builder(&context);
    auto unknownLoc = builder.getUnknownLoc();
    // Circuit description
    auto name = mlir::StringAttr::get(&context, "Shifter");
    auto circuit = builder.create<firrtl::CircuitOp>(unknownLoc, name);

    firrtl::PortInfo inPort{/*Name:*/ mlir::StringAttr::get(&context, "in"),
        /*Type:*/ firrtl::UIntType::get(&context, input_size), 
        /*Direction: */ firrtl::Direction::In};
    firrtl::PortInfo shiftPort{/*Name:*/ mlir::StringAttr::get(&context, "shift"),
        /*Type:*/ firrtl::UIntType::get(&context, shift_size), 
        /*Direction: */ firrtl::Direction::In};

    firrtl::PortInfo outPort{/*Name:*/ mlir::StringAttr::get(&context, "res"),
        /*Type:*/ firrtl::UIntType::get(&context, output_size), 
        /*Direction: */ firrtl::Direction::Out}; 
    
    auto moduleBuilder = circuit.getBodyBuilder();
    auto module = moduleBuilder.create<firrtl::FModuleOp>(unknownLoc, name, llvm::SmallVector<firrtl::PortInfo, 3>{inPort, shiftPort, outPort});
    mlir::Value stage_in = module.getArgument(0);
    auto shift = module.getArgument(1);
    std::size_t cur_shift_size = 1;
    std::size_t shifted_stage_width = input_size; 
    auto opBuilder = module.getBodyBuilder();
    for (std::size_t i = 0 ; i < shift_size ; ++i) {
        shifted_stage_width += cur_shift_size;
        auto cur_stage_res_t = firrtl::UIntType::get(&context, shifted_stage_width);
        auto fill_cst = opBuilder.create<firrtl::ConstantOp>(
            unknownLoc, 
            firrtl::UIntType::get(&context, cur_shift_size), 
            opBuilder.getIntegerAttr(builder.getIntegerType(cur_shift_size, false), 0));
        auto select_bit = opBuilder.create<firrtl::BitsPrimOp>(unknownLoc, shift, i, i);
        auto shifted = opBuilder.create<firrtl::CatPrimOp>(unknownLoc, cur_stage_res_t, stage_in, fill_cst);
        auto unshifted = opBuilder.create<firrtl::CatPrimOp>(unknownLoc, cur_stage_res_t, fill_cst, stage_in);
        stage_in = opBuilder.create<firrtl::MuxPrimOp>(unknownLoc, select_bit, shifted, unshifted);
        cur_shift_size <<= 1;
    }
    opBuilder.create<firrtl::ConnectOp>(unknownLoc, module.getArgument(2), stage_in);
    std::error_code ec{};
    llvm::raw_fd_ostream os{"shifter.mlir", ec};
    circuit->print(os);
    return EXIT_SUCCESS;
}